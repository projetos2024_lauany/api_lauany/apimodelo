const router = require("express").Router(); // Importa o módulo Router do Express
const dbController = require("../controller/dbController"); // Importa o controlador dbController

// Define rotas para consultar tabelas e descrições de tabelas
router.get("/tables", dbController.getNameTables); // Rota para consultar os nomes das tabelas do banco de dados
router.get('/tablesdescriptions', dbController.getTablesDescription); // Rota para consultar as descrições das tabelas do banco de dados

module.exports = router; // Exporta o objeto router com as rotas definidas

