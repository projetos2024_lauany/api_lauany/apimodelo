const connect = require("./connect"); // Importa o módulo de conexão com o banco de dados

module.exports = function testConnect() {
    try {
        const query = `SELECT 'Conexão bem-sucedida' AS Mensagem`; // Define a consulta SQL para verificar a conexão

        // Executa a consulta SQL para testar a conexão com o banco de dados
        connect.query(query, function(err) {
            if (err) {
                console.log("Erro na conexão" + err); // Registra o erro no console se houver problemas na conexão
                return;
            }
            console.log("Conexão realizada com MySQL"); // Registra mensagem de sucesso no console se a conexão for bem-sucedida
        });
    } catch (error) {
        console.error("Erro ao executar a consulta", error); // Registra o erro no console se ocorrer um erro ao executar a consulta
    }
};
