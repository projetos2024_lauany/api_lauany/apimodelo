const mysql = require('mysql2'); // Importa o módulo mysql2

// Cria um pool de conexões MySQL com os parâmetros especificados
const pool = mysql.createPool({
    connectionLimit: 10, // Número máximo de conexões permitidas no pool
    host: 'localhost', // Endereço do host do banco de dados MySQL
    user: 'alunods', // Nome para acessar o banco de dados
    password: 'senai@604', // Senha para acessar o banco de dados
    database: 'reservadequadrasesportivas' // Nome do banco de dados a ser utilizado
});

module.exports = pool; // Exporta o pool de conexões para ser utilizado em outros módulos
